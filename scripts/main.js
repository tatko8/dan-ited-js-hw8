/*

Питання 1. Опишіть своїми словами що таке Document Object Model (DOM) 

Відповідь 1. об`єктна модель документа, що представляє весь зміст сторінки у вигляді об`єктів (кожен HTML-тег є об`єктом), які можна міняти.

Питання 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText? 

Відповідь 2. innerHTML - передає вміст елемента разом з розміткою HTML, innerText передає тільки текст і не несе в собі елементи HTML.

Питання 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Відповідь 3. document.getElement (getElementById, getElementsByClassName, getElementsByTagName) або document.querySelector (querySelectorAll). 
            Вони рівні між собою, використання залежить від цілей пошуку та розмітки. 

*/

"use strict"; //strict mode is a way to introduce better error-checking into your code 

//Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let p = document.querySelectorAll('p');
p.forEach((p) => {
    p.style.backgroundColor = "#ff0000";
});

// Знайти елемент із id="optionsList". Вивести у консоль. 

let optionsList = document.getElementById('optionsList');
console.log(optionsList);

// Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

console.log(optionsList.parentElement);
console.log(optionsList.childNodes);

let optionsListChildrenNodes = optionsList.childNodes;
optionsListChildrenNodes.forEach((childNode) => {
    console.log(`name = ${childNode.nodeName}, type ${childNode.nodeType}.`);
});

//Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

let testParagraph = document.getElementsByClassName('testParagraph');
testParagraph.innerText = 'This is a paragraph';
console.log(testParagraph);

// Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let headerElements = document.querySelector('.main-header');
for(let e of headerElements.children) {
    e.classList.add('nav-item');
    console.log(e); 
};

//Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(e => e.classList.remove('section-title'));
console.log(sectionTitle);
